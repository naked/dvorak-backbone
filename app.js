var io = require('./lib/io.js');
var join = require('./lib/join.js');

io.sockets.on('connection', function(socket) {

  socket.once('join', function(data) {

    // Join that room
    join({ data: data, socket: socket });
  });

  socket.on('card:target', function(id) {

    var target_player, in_play, target_card, active_player;

    // Get out of here if we dont have a game
    if(!socket.game) {
      return;
    }

    active_player = socket.game.getActivePlayer();

    // Is it the player's turn?
    if(socket.player === active_player){
        console.log('card target id:', id);

      // Find target card and player
      socket.game.players.forEach(function(player) {
        if(target_card) return;

        target_player = player;
        target_card = player.in_play.get(id);
      });

      // Discard the target card from being in play
      target_card = target_player.in_play.remove(target_card);
      socket.game.discardPile.add(target_card);

      // Cards in play changed, time to update everyone
      socket.game.serverUpdatePlayers();

    } else {
      console.log('not your turn');
      socket.emit('info', 'not your turn');
    }
  });

  socket.on('queue:card:cancel', function(id) {
    // Get out of here if we dont have a game
    if(!socket.game) {
      return;
    }

    // Put card back in hand from queue
    var card;
    card = socket.game.playQueue.get(id);
    socket.game.cancelPlay(card);
  });

  socket.on('card:play', function(data) {

    var target_player, target_card, active_player, card;

    // data is an object with 2 properties
    // { played: id, target: id }
    // Played is the card form players hand, target
    // is an optional target for card's action

    // Get out of here if we dont have a game
    if(!socket.game) {
      return;
    }

    active_player = socket.game.getActivePlayer();

    // Is it the player's turn?
    if(socket.player === active_player){

      // remove card from hand
      card = socket.player.hand.remove(socket.player.hand.get(data.played));

      // If there's a target, find and attach to card
      if(data.target) {

        // Find target card and player
        socket.game.players.forEach(function(player) {
          if(target_card) return;

          target_player = player;
          target_card = player.in_play.get(data.target);
        });

        card.set('target', target_card);
      }

      // Add card to playQueue, which triggers some validations
      socket.game.playQueue.add(card);

    } else {
      console.log('not your turn');
    }

  });

  socket.on('player:finishturn', function() {

    if(!socket.game) {
      return;
    }

    var active_player = socket.game.getActivePlayer();
    if(socket.player === active_player) {

      // Turn over, rotate players
      socket.game.rotatePlayers();
      console.log('rotated players');
    } else {
      console.log('not your turn');
    }
  });

  socket.on('players:read', function(data) {

    // Make sure we have a game before we try to read it
    if(!socket.game){
      return;
    } 

    var players = socket.game.players.getPublicPlayers();
    socket.emit('players:update', players);

  });

  socket.on('disconnect', function() {
    if(socket.game) {
      var discardPile = socket.game.discardPile;
      var room_name = socket.game.get('name'); 

      console.log('disconnect');

      // Move all cards in play to the discard pile
      discardPile.add(socket.player.hand.models);
      discardPile.add(socket.player.in_play.models);

      // Remove player from game's players collection
      socket.game.players.remove(socket.player);
    }
  });
});
