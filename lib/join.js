var Game = require('./game.js');
var player = require('./player.js');
var io = require('./io.js');
var RSVP = require('rsvp');

// Actually crash on errors!
RSVP.configure('onerror', function(e) { throw(e); });

module.exports = function(args) {

  var player_name = args.data.player_name;
  var room_name = args.data.room_name;
  var socket = args.socket;

  var join_promise = new RSVP.Promise(function(resolve, reject) {

    var new_game;

    // Add a backbone player model to our socket object
    socket.player = new player.Player({name: player_name});

    // Have socket join the room
    socket.join(room_name);

    // Attach reference to socket to player (circular reference)
    socket.player.socket = socket;

    // If our backbone socket.game is already attached to room
    if(io.sockets.manager.rooms["/" + room_name]["game"]) {
      console.log("game EXISTS");
      socket.game = io.sockets.manager.rooms["/" + room_name]["game"];

      socket.emit('info', "Welcome! You may play 1 action and 1 thing per turn.");

      resolve();

    // Else create one
    } else {
      console.log("game does not EXIST");

      // Attach new Game model to room
      new_game = new Game({ id: '0', name: room_name });
      socket.game = new_game;
      io.sockets.manager.rooms["/" + room_name]["game"] = socket.game;

      socket.emit('info', "Welcome! You may play 1 action and 1 thing per turn.");

      resolve();

    }
  });

  // Resolve the promise
  join_promise.then(function() {
    var table_info, player_info;

    // Set player model id to socket id
    socket.player.set("id", socket.id.substring(0,5)); 
    
    // Add player to game
    socket.game.addPlayer(socket.player);

    // Emit 'game ready' event
    socket.emit('game:ready');


  });

}
