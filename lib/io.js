var connect = require('connect');
var http = require('http');

var app = connect();
var server = http.createServer(app);  

io = require('socket.io').listen(server);
io.set('log level', 2);

app.use(connect.static(__dirname + "/../public"));

server.listen(3000);

module.exports = io;
