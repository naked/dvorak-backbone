ctor = function(socket) {

  this.hasGame = function() {
    if(!socket.game) {}
    return this;
  }

  this.isYourTurn = function() {

    var active_player = socket.game.getActivePlayer();

    // Is it the player's turn?
    if(socket.player === active_player){
    }

    return this;
  }

  this.check = function() {
    return true;
  }
}

// Export constructor
module.exports = ctor;
