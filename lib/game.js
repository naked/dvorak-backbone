var Backbone = require('backbone');
var _ = require('underscore');
var io = require('./io.js');
var rules = require('./rules.js');
var player = require('./player.js');
var card = require('./cards.js');

var Game = Backbone.Model.extend({
  urlRoot: "game",

  initialize: function() {

    // Setup collections belonging to the game
    this.drawDeck = card.drawDeckFactory(card.deck);
    this.players = new player.Players();
    this.discardPile = new card.Pile();
    this.playQueue = new card.Pile();

    // Setup our event listeners on our players collection
    this.listenTo(this.players, "add", this.serverAddPlayer);
    this.listenToOnce(this.players, "add", this.dealCard);

    // Event listeners for our playQueue
    this.listenTo(this.playQueue, "add", this.validatePlay);
    this.listenTo(this.playQueue, "remove", this.cancelPlay);

    // Event listener on our discardPile and drawDeck
    this.listenTo(this.discardPile, "add remove reset", this.updateTable);
    this.listenTo(this.drawDeck, "add remove reset", this.updateTable);
  },
  winner: function(player) {
    // TODO: Actually program what happens when someone wins
    console.log('A winner is: ' + player.get('name'));
    //io.sockets.in(room_name).emit('players:create', player.getPublicPlayer());
  },
  dealCard: function(player) {

    console.log('drawDeck length', this.drawDeck.length);

    // If we don't have any cards to draw
    if(this.drawDeck.length === 0){

      // Shuffle the discardPile and put all the cards into the draw deck
      this.drawDeck.reset(_.shuffle(this.discardPile.models));

      // Empty the collection
      this.discardPile.reset();

    }

    // Deal next player card
    player.hand.add(this.drawDeck.shift());

  },

  rotatePlayers: function() {

    // Run cards in play queue throgh rules
    rules.process(this);
  },

  cancelPlay: function(card) {
    var player = this.getActivePlayer();

    // TODO: Make this better instansiate: Queue pile with general listeners
    player.socket.emit('queue:delete', card);
    player.hand.add(this.playQueue.remove(card));
  },

  validatePlay: function(card) {

    // Get active player
    var player = this.getActivePlayer();

    // get the # of cards of this type in the play queue already
    var cards_of_same_type = this.playQueue.where({ type: card.get("type") });

    // Only allowed to play one card of each type during your turn
    if(cards_of_same_type.length > 1) {

      // remove the previously selected card of type to play from queue
      this.playQueue.remove(cards_of_same_type[0]);

      // add back to player's hand
      player.hand.add(cards_of_same_type[0]);

      // Emit events to sync models
      player.socket.emit('queue:delete', cards_of_same_type[0]);
      player.socket.emit('queue:create', card);

      console.log('swap');
    } else {
      player.socket.emit('queue:create', card);
      console.log('valid');
    }
  },

  getActivePlayer: function() {
    return this.players.at(0);
  },

  serverUpdatePlayers: function() {
    console.log('serverUpdatePlayers');
    var room_name = this.get("name");
    io.sockets.in(room_name).emit('players:update', this.players.getPublicPlayers()); 
  },

  serverAddPlayer: function(player) {
    console.log('serverAddPlayer');

    // Update players
    var room_name = this.get("name");
    io.sockets.in(room_name).emit('players:create', player.getPublicPlayer());
  },

  updateTable: function(data) {
    console.log('updateTable');

    // Update table info
    var room_name = this.get("name");
    io.sockets.in(room_name).emit('table/0:update', this.getTableInfo() );
  },

  addPlayer: function(player) {

    // Draw 5 cards
    var drawDeck = this.drawDeck;
    var fiveCards = drawDeck.remove(drawDeck.slice(0,5));

    // Set the cards to player's hand collection
    player.hand.reset(fiveCards);

    // Add model to collection
    this.players.add(player);

    // Update table
    this.updateTable();
  },

  getTableInfo: function() {
    return {
      name: this.get("name"),
      draw_quantity: this.drawDeck.length,
      discard_quantity: this.discardPile.length
    }
  }

});

module.exports = Game;
