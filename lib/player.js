var Backbone = require('backbone');
var _ = require('underscore');
var card = require('./cards.js');

var Player = Backbone.Model.extend({
  urlRoot: "player",
  initialize: function() {

    // Create empty collections for hand and cards in play
    this.in_play = new card.Pile();
    this.hand = new card.Pile();

    // Event listeners on our hand
    this.listenTo(this.hand, 'remove', this.removeFromHand);
    this.listenTo(this.hand, 'add', this.addToHand);
    this.listenTo(this.hand, 'reset', this.updateHand);

    // Change events
    this.listenTo(this.in_play, 'add', this.addInPlay);

  },
  addInPlay: function(card) {
    var player_id = this.socket.player.get('id');
    var room_name = this.socket.game.get('name');
    io.sockets.in(room_name).emit(player_id + '-inplay:create', card);
  },
  updateHand: function(hand) {

    // Emit hand collection to player
    this.socket.emit('hand:update', hand);
  },
  addToHand: function(card) {
    console.log('Player add to hand emit');
    this.socket.emit('hand:create', card);
  },
  removeFromHand: function(card) {
    console.log('emitting hand:delete');
    this.socket.emit('hand:delete', card);
  },
  getPublicPlayer: function() {
    var publicPlayer = {
      id: this.get("id"),
      name: this.get("name"),
      in_play: this.in_play,
      hand_quantity: this.hand.length,
    }
    return publicPlayer;
  }
});

var Players = Backbone.Collection.extend({
  model: Player,
  initialize: function() {
    this.on('remove', this.serverDelete);
    this.on('add', this.serverCreate);
  },
  serverCreate: function(player) {
    console.log('serverCreate!');
  },
  serverDelete: function(player) {
    console.log('emitting Players serverDelete');
    var room_name = player.socket.game.get('name');
    io.sockets.in(room_name).emit('players:delete', player);
  },
  getPublicPlayers: function() {
    var publicPlayers = this.models.map(function(player) {
      return player.getPublicPlayer();
    });
    return publicPlayers;
  }
});

module.exports = {
  Player: Player,
  Players: Players,
}
