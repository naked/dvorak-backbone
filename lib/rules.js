var process = function(game) {
    var i, winner, card, target, type, hand;
    var active_player = game.getActivePlayer();
    var queue = game.playQueue;
    var queue_length = queue.length;

    // Loop through cards queued to be processed
    for(i = 0; i < queue_length; i++) {

      card = queue.pop();
      type = card.get('type');

      if(type === 'action') {

        target = card.get("target");
        game.players.each(function(player) {
          game.discardPile.add(player.in_play.remove(target));
        });

        game.discardPile.add(card);

      } else if(type === 'thing') {
        active_player.in_play.add(card);
      }
    }

    // Discard down to 5 cards in hand if necessary
    hand = active_player.hand;

    if(hand.length > 5) {
      game.discardPile.add(hand.remove(hand.slice(5, hand.length)));
    }

    // check if game meets the winner condition
    winner = game.players.find(function(player) {
      return player.in_play.length > 5;
    });

    if(winner) {

      game.winner(player);
    } else {

      // Rotate the player stack
      var turnOver = game.players.shift({silent: true});
      game.players.push(turnOver, {silent: true});

      // Draw card for the next guy
      game.dealCard(game.getActivePlayer());

      // Update everyone's player view
      game.serverUpdatePlayers();
    }


}

module.exports = {
  process: process
}
