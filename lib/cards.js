var Backbone = require('backbone');
var _ = require('underscore');

// Extend the card model with methods for 
var CardModel = Backbone.Model.extend({
  addToQueue: function() {
    console.log('addToQueue');
    return this;
  },

  idk: function() {
    console.log('idk');
  },

  fuck: function() {
    console.log('fuck');
  },
});

// Card pile collection, to be extended by hand, deck, etc
var CardPile = Backbone.Collection.extend({
  model: CardModel
});

// Create a deck of cards
var drawDeckFactory = function(cards) {

  var i, deck, shuffled, id;

  deck = [];

  // Assign each card it's unique ID
  id = 0;

  _.each(cards, function(card) {
    for(i = 0; i < card.quantity; i++){
      card.id = id;
      deck.push(new CardModel(card));
      id += 1;
    }
  });

  shuffled = _.shuffle(deck);

  deckCollection = new CardPile(shuffled);
  return deckCollection;
}


var deck = [
  {
    title: 'Action', 
    type: 'action',
    quantity: 10,
    description: 'A perfectly generic action, remove a thing in play',
    poop: function() {
      console.log('ACTION poop');
      return this;
    },

    pee: function() {
      console.log('ACTION pee');
      return this;
    },

    what: function() {
      console.log('ACTION what');
      return this;
    }
  },
  {
    title: 'Thing',
    type: 'thing',
    quantity: 30,
    description: 'A perfectly generic thing, adds one to player\'s thing count when in play',
    poop: function() {
      console.log('ACTION poop');
      return this;
    },
  }
];

module.exports = {
  deck: deck,
  drawDeckFactory: drawDeckFactory,
  Model: CardModel,
  Pile: CardPile,
}
