App.Models.Player = Backbone.Model.extend({
  urlRoot: 'player',
  initialize: function() {
    _.bindAll(this, 'serverUpdate');
    this.ioBind('update', window.socket, this.serverUpdate, this);
  },
  serverUpdate: function(data) {
    debug.log('PLAYER model set');
    this.set(data);
  }
});

App.Views.Player = Backbone.View.extend({
  className: 'player-info',
  template: _.template($('#player-template').html()),
  initialize: function() {
    this.model.on("change", this.render, this);

    // Determine if player is "you", compare to sessionid substr
    if(this.model.id === App.game.getMyPlayerId()) {
      this.you = true;
    }

  },
  render: function() {
    debug.log('player view render');
    this.$el.html(this.template(this.model.attributes));
    return this;
  }
});

App.Views.Players = Backbone.View.extend({
  el: '#players-container',
  template: _.template($('#players-template').html()),
  initialize: function(options) {
    var that = this;

    this.collection.on('add', this.addOne, this);
    this.collection.on('reset', this.addAll, this);

    // Get players in room after joining a game
    socket.on('game:ready', function() {
      that.collection.fetch();
    });

  },
  render: function() {
    debug.log('PlayersView render');
    this.addAll();
    return this;
  },
  addAll: function() {
    debug.log('PlayersView addAll');
    this.$el.empty();
    this.$el.html(this.template());
    this.collection.each(this.addOne, this);
  },
  addOne: function(model) {
    debug.log('PlayersView addOne');
    var inPlayCollection, playerView, inPlayView, removeViews;

    removeViews = function() {
      playerView.remove();
      inPlayView.remove();
    }

    // Create our collection of cards in play for player
    inPlayCollection = new App.Collections.CardPile(model.get('in_play'), {url: 'inplay'});

    // Create bother Player and inPlay views
    playerView = new App.Views.Player({model: model});
    inPlayView = new App.Views.InPlay({collection: inPlayCollection });

    // Render them both
    playerView.render();
    inPlayView.render();

    // Append them
    this.$el.append(playerView.el);
    playerView.$el.append(inPlayView.el);

    model.on('remove', removeViews);
  }
});

App.Collections.Players = Backbone.Collection.extend({
  url: 'players',
  model: App.Models.Player,
  initialize: function() {
    _.bindAll(this, 'serverCreate', 'serverUpdate', 'serverDelete');
    this.ioBind('create', window.socket, this.serverCreate, this);
    this.ioBind('update', window.socket, this.serverUpdate, this);
    this.ioBind('delete', window.socket, this.serverDelete, this);
  },
  serverCreate: function(data) {
    debug.log('data ', data);

    var player = new App.Models.Player(data);
    this.add(player);
  },
  serverUpdate: function(data) {
    debug.log('PLAYERS collection reset');
    this.reset(data);
  },
  serverDelete: function(player) {
    debug.log('players serverDelete');
    this.remove(player);
  }
});
