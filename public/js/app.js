// Debug messages: true = on, false = off
window.debug.enable = false;

// Connect to socket.io
window.socket = io.connect("http://" + window.location.host);

// Global object holding our backbone vertabrae
window.App = {
  Models: {},
  Collections: {},
  Views: {}
};

// On page ready
$(function() {

  // Globally accessable instances
  window.App.game = new App.Models.Game({id: 0});
  window.App.players = new App.Collections.Players();

  // Create instances
  new App.Views.Hand({ collection: new App.Collections.CardPile({ url: "hand" }) });

  new App.Views.Sidebar().render();

  new App.Views.Game({ model: App.game });

  new App.Views.Queue({ collection: new App.Collections.CardPile({ url: "queue" }) });

  new App.Views.Players({ collection: App.players });

  // TODO: Turn these event listeners into a view (maybe?)
  // TODO: Make sure it's your turn
  $('#finish-turn-button').click(function() {
    debug.log('finish turn clicked');

    // Emit the finish turn event
    socket.emit('player:finishturn');
  });

  $('#join-form').submit(function(e) {
    e.preventDefault();

    var player_name = $('#player-name').val();
    var room_name = $('#table-name').val();

    if(player_name.length > 0 &&
      room_name.length  > 0 &&
      room_name.length  <= 12 &&
      player_name.length <= 12) 
    {

      socket.emit('join', { player_name: player_name, room_name: room_name });
      $('#join-div').hide();

    } else {

      $('#game-info').html('Enter a room and player name to join!');

    }
  });

  // Socket.io listener for game info messages
  socket.on('info', function(message) {
    $('#game-info').html(message);
  });

});
