// Card Model
App.Models.Card = Backbone.Model.extend({

  url: 'card',
  initialize: function() {
    _.bindAll(this, 'serverUpdate');
    this.ioBind('update', window.socket, this.serverUpdate, this);
  },
  serverUpdate: function(data) {
    debug.log('serverUpdate Card');
    this.set(data);
  },
  play: function() {
    var data = {};
    var _this_card = this;

    // Set played to this card's id
    data.played = this.id;

    if(App.game.myTurn()) {
      if(this.get("type") === 'action') {


          $('#game-info').html('Click a card in play to target');

          $('.small-card').one('click', function() {
            $('#game-info').html('TARGETED CARD! Well done~');

            // Set data.target to the target card's id
            data.target = $(this).prop('id');

            // Stop listening for click events
            $('.small-card').off('click');

            // Emit event after targeting
            socket.emit('card:play', data);
          });

      // Don't wait to emit the play event if you don't need to target
      } else {

        // Emit event with data about play
        socket.emit('card:play', data);
      }

    }
  }
});

App.Views.Card = Backbone.View.extend({
  className: "card",
  template: _.template($('#card-template').html()),
  events: {
    "click": "play",
    "mouseenter": "animate",
    "mouseleave": "stopanimation"
  },
  initialize: function() {
    this.model.on("change", this.render, this);
  },
  fadeRemove: function() {
    debug.log('remove card view');
    var that = this;
    this.$el.fadeOut(200, function() {
      that.remove();
    });
  },
  render: function() {
    debug.log('render card:', this.model.attributes);
    this.$el.html(this.template(this.model.attributes));
    return this;
  },
  animate: function() {
    var card_img = this.$el.find('.card-img');
    card_img.removeClass('card-still');
    card_img.addClass(this.model.get('type') + '-animate');
  },
  stopanimation: function() {
    var card_img = this.$el.find('.card-img');
    card_img.removeClass(this.model.get('type') + '-animate');
    card_img.addClass('card-still');
  },
  play: function() {

    if(App.game.myTurn()) {
      this.model.play();
    }
  }
});
