App.Views.Hand = Backbone.View.extend({
  el: "#hand-container",
  template: _.template($('#hand-template').html()),
  initialize: function(options) {
    this.collection.on('add', this.addOne, this);
    this.collection.on('reset', this.addAll, this);
  },
  render: function() {
    debug.log('HandView render fired');
    this.addAll();
    return this;
  },
  addOne: function(model) {
    debug.log('HandView addOne fired');
    view = new App.Views.Card({model: model});
    view.render();
    this.$el.append(view.el);
    model.on('remove', view.fadeRemove, view);
  },
  addAll: function() {
    debug.log('HandView addAll fired');
    this.$el.empty();
    this.$el.html(this.template());
    this.collection.each(this.addOne, this);
  }
});
