App.Views.InPlay = Backbone.View.extend({
  className: "cf",
  template: _.template($('#inplay-template').html()),
  initialize: function(options) {
    debug.log('collection:', this.collection);
  },
  render: function() {
    debug.log('InPlay render to:', this.el);
    this.addAll();
    return this;
  },
  addAll: function() {
    debug.log('InPlay addAll');
    this.$el.empty();
    this.$el.html(this.template());
    this.collection.each(this.addOne, this);
  },
  addOne: function(card) {
    var view;
    debug.log('InPlay addOne:', card);
    view = new App.Views.InPlayCard({model: card});
    view.render();
    this.$el.append(view.el);
    card.on('remove', view.remove, view);
  }
});

App.Views.InPlayCard = Backbone.View.extend({
  template: _.template($('#small-card-template').html()),
  className: "small-card",
  events: {
    "click": "target"
  },
  initialize: function() {
  },
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    this.$el.prop('id', this.model.get('id'));
    return this;
  }
});
