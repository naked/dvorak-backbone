App.Views.Queue = Backbone.View.extend({
  template: _.template($('#inplay-template').html()),
  render: function() {
    debug.log('Queue render to:', this.el);
    this.addAll();
    return this;
  },
  addAll: function() {
    debug.log('Queue addAll');
    this.$el.empty();
    this.$el.html(this.template());
    this.collection.each(this.addOne, this);
  },
  addOne: function(card) {
    var view;
    debug.log('Queue addOne:', card);
    view = new App.Views.QueueCard({model: card});
    view.render();
    this.$el.append(view.el);
    card.on('remove', view.remove, view);
  }
});

App.Views.QueueCard = Backbone.View.extend({
  template: _.template($('#small-card-template').html()),
  className: "small-card",
  events: {
    "click": "cancel"
  },
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    return this;
  },
  cancel: function() {

    // TODO: Only if your turn
    socket.emit('queue:card:cancel', this.model.get('id'));
  }
});
