App.Models.Game = Backbone.Model.extend({
  urlRoot: 'table',
  initialize: function() {
    _.bindAll(this, 'serverUpdate');
    this.ioBind('update', window.socket, this.serverUpdate, this);
    this.ioBind('setActivePlayer', window.socket, this.serverUpdate, this);
  },
  getMyPlayerId: function() {
    return socket.socket.sessionid.substr(0,5);
  },
  getActivePlayerId: function() {
    return App.players.at(0).get('id');
  },
  myTurn: function() {
    return (this.getMyPlayerId() === this.getActivePlayerId());
  },
  serverUpdate: function(data) {
    this.set(data);
  }
});

App.Views.Game = Backbone.View.extend({
  el: '#table-container', 
  template: _.template($('#table-template').html()),
  initialize: function() {
    this.listenTo(this.model, "change", this.render);
  },
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    return this;
  }
});

App.Views.Queue = Backbone.View.extend({
  el: '#queue-container', 
  initialize: function(options) {
    this.collection.on('add', this.addOne, this);
    this.collection.on('reset', this.addAll, this);
  },
  render: function() {
    debug.log('Queue render to:', this.el);
    this.addAll();
    return this;
  },
  addAll: function() {
    debug.log('Queue addAll');
    this.$el.empty();
    this.collection.each(this.addOne, this);
  },
  addOne: function(card) {
    var view;
    debug.log('Queue addOne:', card);
    view = new App.Views.QueueCard({model: card});
    view.render();
    this.$el.append(view.el);
    card.on('remove', view.remove, view);
  }
});

App.Views.QueueCard = Backbone.View.extend({
  template: _.template($('#small-card-template').html()),
  className: "small-card",
  events: {
    "click": "cancel"
  },
  render: function() {
    this.$el.html(this.template(this.model.attributes));
    return this;
  },
  cancel: function() {

    // TODO: Only if your turn
    socket.emit('queue:card:cancel', this.model.get('id'));
  }
});

App.Views.Sidebar = Backbone.View.extend({
  el: "#right-sidebar",
  template: _.template($('#sidebar-template').html()),
  initialize: function() {
    this.listenTo(App.players, 'reset', this.checkTurnStatus);
  },
  checkTurnStatus: function() {

    // Do stuff to sidebar based on whether it's your turn or not
    if(App.game.myTurn()) {
      $('#finish-turn-button').prop('disabled', false);
      $('#game-info').html('It\'s your turn! You can play 1 thing and 1 action.');
    } else {
      $('#finish-turn-button').prop('disabled', true);
      $('#game-info').html('Wait patiently for your turn to begin, the active player is the first one listed on the left there, you\'ll be on top soon enough!');
    }
  },
  render: function() {
    this.$el.html(this.template());
    return this;
  }
});
