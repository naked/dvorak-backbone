// Generic card pile
App.Collections.CardPile = Backbone.Collection.extend({
  model: App.Models.Card,
  initialize: function(options) {

    // Set url from options
    this.url = options.url;

    _.bindAll(this, 'serverCreate', 'serverUpdate', 'serverDelete');
    this.ioBind('create', window.socket, this.serverCreate, this);
    this.ioBind('update', window.socket, this.serverUpdate, this);
    this.ioBind('delete', window.socket, this.serverDelete, this);
  },
  serverCreate: function(card) {
    this.add(card);
  },
  serverUpdate: function(data) {
    debug.log('CardPile: serverUpdate:', this.url);
    this.reset(data);
  },
  serverDelete: function(card) {
    debug.log('CardPile: serverDelete:', this.url);
    this.remove(card);
  }
});
